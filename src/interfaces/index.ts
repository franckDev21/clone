export interface IRestaurant {
  id: number;
  attributes: {
    Name: string;
    Ratings: number;
    Reviews: number;
    Location: string;
    Description: string;
  };
}
