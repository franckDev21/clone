import React, { useEffect, useRef } from "react";
import { Navigation, Pagination, Scrollbar, A11y } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "../../assets/styles/slider.css";
import testimonial1 from "../../assets/fr_testimonial_1-9b8d74271bbb512bdc0f.jpg";
import testimonial2 from "../../assets/fr_testimonial_2-4a17233e637e6f55aa77.avif";
import testimonial3 from "../../assets/fr_testimonial_3-f8af15d656bd126cd1f7.avif";
import { IoIosArrowForward, IoIosArrowBack } from "react-icons/io";

const SectionSlider = () => {
  return (
    <div className="mySwipper">
      <div className="swiper-button image-swiper-button-next">
        <IoIosArrowForward />
      </div>
      <div className="swiper-button image-swiper-button-prev">
        <IoIosArrowBack />
      </div>
      <Swiper
        effect="fade"
        navigation={{
          nextEl: ".image-swiper-button-next",
          prevEl: ".image-swiper-button-prev",
          disabledClass: "swiper-button-disabled",
        }}
        modules={[Navigation]}
        className="mySwiper"
      >
        <SwiperSlide>
          <div
            className="w-screen px-8 lg:px-32 h-[950px] lg:h-[550px] flex lg:flex-row flex-col  justify-between z-0"
            style={{ backgroundColor: "#9DA9F3" }}
          >
            <div className="w-full p-4 mt-5 lg:hidden block">
              <p className="font-bold text-2xl text-center">
                Nos etudiants temoignent
              </p>
            </div>
            <div className="w-full lg:w-1/3 bg-white">
              <img
                src={testimonial1}
                alt=""
                className="object-cover w-full relative h-full"
              />
            </div>
            <div className="w-full lg:w-2/3 flex flex-col pb-20 p-2 lg:p-10 lg:mt-28">
              <p className="font-bold text-2xl hidden lg:block">
                {" "}
                Nos etudiants temoignent{" "}
              </p>
              <p className="font-bold text-xl pt-4">Chloe</p>
              <p className="font-bold text-lg">
                {" "}
                Conseiller emploi et insertion professionelle{" "}
              </p>
              <p className="text-md pt-2">
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book. It has
              </p>
            </div>
          </div>
        </SwiperSlide>

        <SwiperSlide>
          <div
            className="w-screen px-8 lg:px-32 h-[950px] lg:h-[550px] flex lg:flex-row flex-col  justify-between z-0"
            style={{ backgroundColor: "#F2E93F" }}
          >
            <div className="w-full p-4 mt-5 lg:hidden block">
              <p className="font-bold text-2xl text-center">
                Nos etudiants temoignent
              </p>
            </div>
            <div className="w-full lg:w-1/3 bg-white">
              <img
                src={testimonial2}
                alt=""
                className="object-cover w-full relative h-full"
              />
            </div>
            <div className="w-full lg:w-2/3 flex flex-col pb-20 p-2 lg:p-10 lg:mt-28">
              <p className="font-bold text-2xl hidden lg:block">
                {" "}
                Nos etudiants temoignent{" "}
              </p>
              <p className="font-bold text-xl pt-4">Chloe</p>
              <p className="font-bold text-lg">
                {" "}
                Conseiller emploi et insertion professionelle{" "}
              </p>
              <p className="text-md pt-2">
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book. It has
              </p>
            </div>
          </div>
        </SwiperSlide>

        <SwiperSlide>
          <div
            className="w-screen px-8 lg:px-32 h-[950px] lg:h-[550px] flex lg:flex-row flex-col  justify-between z-0"
            style={{ backgroundColor: "#F9AB2D" }}
          >
            <div className="w-full p-4 mt-5 lg:hidden block">
              <p className="font-bold text-2xl text-center">
                Nos etudiants temoignent
              </p>
            </div>
            <div className="w-full lg:w-1/3 bg-white">
              <img
                src={testimonial3}
                alt=""
                className="object-cover w-full relative h-full"
              />
            </div>
            <div className="w-full lg:w-2/3 flex flex-col pb-20 p-2 lg:p-10 lg:mt-28">
              <p className="font-bold text-2xl hidden lg:block">
                {" "}
                Nos etudiants temoignent{" "}
              </p>
              <p className="font-bold text-xl pt-4">Chloe</p>
              <p className="font-bold text-lg">
                {" "}
                Conseiller emploi et insertion professionelle{" "}
              </p>
              <p className="text-md pt-2">
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book. It has
              </p>
            </div>
          </div>
        </SwiperSlide>
      </Swiper>
    </div>
  );
};

export default SectionSlider;
