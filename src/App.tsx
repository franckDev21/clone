import React, { useState, useContext, useEffect } from "react";
import { ChakraProvider } from "@chakra-ui/react";

import Home from "./screens/Home/Home";

function App() {
  // Utilisation du context

  return (
    <ChakraProvider>
      <div className="w-screen h-screen flex flex-col relative font-poppins">
        <Home />
      </div>
    </ChakraProvider>
  );
}

export default App;
