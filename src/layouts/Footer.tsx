import React from "react";
import { HiOutlineExternalLink } from "react-icons/hi";
import { AiFillMail } from "react-icons/ai";
import {
  Accordion,
  AccordionItem,
  AccordionButton,
  AccordionPanel,
  AccordionIcon,
  Box,
} from "@chakra-ui/react";

const Footer: React.FC = () => {
  return (
    <div className="p-4 lg:p-10 bg-white w-full flex lg:grid lg:grid-cols-4 lg:gap-8">
      <div className="lg:hidden flex w-full pr-5">
        <Accordion defaultIndex={[0]} allowMultiple className="w-full">
          <AccordionItem>
            <h2>
              <AccordionButton>
                <Box as="span" flex="1" textAlign="left">
                  <p className="text-md uppercase pb-3 hover:text-indigo-600">
                    Openclassrooms
                  </p>
                </Box>
                <AccordionIcon />
              </AccordionButton>
            </h2>
            <AccordionPanel pb={4} className="flex flex-col space-y-6">
              <span className="text-sm hover:text-indigo-600 flex flex-row space-x-3 text-slate-700">
                {" "}
                Qui sommes nous
              </span>
              <span className="text-sm hover:text-indigo-600 flex flex-row space-x-3 text-slate-700">
                {" "}
                Financements
              </span>
              <span className="text-sm hover:text-indigo-600 flex flex-row space-x-3 text-slate-700">
                Experience de formation
              </span>
              <span className="text-sm hover:text-indigo-600 flex flex-row space-x-3 text-slate-700">
                {" "}
                Forum
              </span>
              <span className="text-sm hover:text-indigo-600 flex flex-row space-x-3 text-slate-700">
                {" "}
                Blog{" "}
                <HiOutlineExternalLink
                  style={{ marginTop: "2px ", marginLeft: "5px" }}
                />
              </span>
              <span className="text-sm hover:text-indigo-600 flex flex-row space-x-3 text-slate-700">
                {" "}
                Presse{" "}
                <HiOutlineExternalLink
                  style={{ marginTop: "2px ", marginLeft: "5px" }}
                />
              </span>
            </AccordionPanel>
          </AccordionItem>
          <AccordionItem>
            <h2>
              <AccordionButton>
                <Box as="span" flex="1" textAlign="left">
                  <p className="text-md uppercase pb-3 hover:text-indigo-600">
                    Openclassrooms
                  </p>
                </Box>
                <AccordionIcon />
              </AccordionButton>
            </h2>
            <AccordionPanel pb={4} className="flex flex-col space-y-6">
              <span className="text-sm hover:text-indigo-600 flex flex-row space-x-3 text-slate-700">
                {" "}
                Qui sommes nous
              </span>
              <span className="text-sm hover:text-indigo-600 flex flex-row space-x-3 text-slate-700">
                {" "}
                Financements
              </span>
              <span className="text-sm hover:text-indigo-600 flex flex-row space-x-3 text-slate-700">
                Experience de formation
              </span>
              <span className="text-sm hover:text-indigo-600 flex flex-row space-x-3 text-slate-700">
                {" "}
                Forum
              </span>
              <span className="text-sm hover:text-indigo-600 flex flex-row space-x-3 text-slate-700">
                {" "}
                Blog{" "}
                <HiOutlineExternalLink
                  style={{ marginTop: "2px ", marginLeft: "5px" }}
                />
              </span>
              <span className="text-sm hover:text-indigo-600 flex flex-row space-x-3 text-slate-700">
                {" "}
                Presse{" "}
                <HiOutlineExternalLink
                  style={{ marginTop: "2px ", marginLeft: "5px" }}
                />
              </span>
            </AccordionPanel>
          </AccordionItem>
          <AccordionItem>
            <h2>
              <AccordionButton>
                <Box as="span" flex="1" textAlign="left">
                  <p className="text-md uppercase pb-3 hover:text-indigo-600">
                    Openclassrooms
                  </p>
                </Box>
                <AccordionIcon />
              </AccordionButton>
            </h2>
            <AccordionPanel pb={4} className="flex flex-col space-y-6">
              <span className="text-sm hover:text-indigo-600 flex flex-row space-x-3 text-slate-700">
                {" "}
                Qui sommes nous
              </span>
              <span className="text-sm hover:text-indigo-600 flex flex-row space-x-3 text-slate-700">
                {" "}
                Financements
              </span>
              <span className="text-sm hover:text-indigo-600 flex flex-row space-x-3 text-slate-700">
                Experience de formation
              </span>
              <span className="text-sm hover:text-indigo-600 flex flex-row space-x-3 text-slate-700">
                {" "}
                Forum
              </span>
              <span className="text-sm hover:text-indigo-600 flex flex-row space-x-3 text-slate-700">
                {" "}
                Blog{" "}
                <HiOutlineExternalLink
                  style={{ marginTop: "2px ", marginLeft: "5px" }}
                />
              </span>
              <span className="text-sm hover:text-indigo-600 flex flex-row space-x-3 text-slate-700">
                {" "}
                Presse{" "}
                <HiOutlineExternalLink
                  style={{ marginTop: "2px ", marginLeft: "5px" }}
                />
              </span>
            </AccordionPanel>
          </AccordionItem>
          <AccordionItem>
            <h2>
              <AccordionButton>
                <Box as="span" flex="1" textAlign="left">
                  <p className="text-md uppercase pb-3 hover:text-indigo-600">
                    Openclassrooms
                  </p>
                </Box>
                <AccordionIcon />
              </AccordionButton>
            </h2>
            <AccordionPanel pb={4} className="flex flex-col space-y-6">
              <span className="text-sm hover:text-indigo-600 flex flex-row space-x-3 text-slate-700">
                {" "}
                Qui sommes nous
              </span>
              <span className="text-sm hover:text-indigo-600 flex flex-row space-x-3 text-slate-700">
                {" "}
                Financements
              </span>
              <span className="text-sm hover:text-indigo-600 flex flex-row space-x-3 text-slate-700">
                Experience de formation
              </span>
              <span className="text-sm hover:text-indigo-600 flex flex-row space-x-3 text-slate-700">
                {" "}
                Forum
              </span>
              <span className="text-sm hover:text-indigo-600 flex flex-row space-x-3 text-slate-700">
                {" "}
                Blog{" "}
                <HiOutlineExternalLink
                  style={{ marginTop: "2px ", marginLeft: "5px" }}
                />
              </span>
              <span className="text-sm hover:text-indigo-600 flex flex-row space-x-3 text-slate-700">
                {" "}
                Presse{" "}
                <HiOutlineExternalLink
                  style={{ marginTop: "2px ", marginLeft: "5px" }}
                />
              </span>
            </AccordionPanel>
          </AccordionItem>
        </Accordion>
      </div>

      <div className="hidden lg:flex flex-col space-y-3">
        <p className="text-md uppercase pb-3 hover:text-indigo-600">
          {" "}
          Openclassrooms
        </p>
        <span className="text-sm hover:text-indigo-600 flex flex-row space-x-3 text-slate-700">
          {" "}
          Qui sommes nous
        </span>
        <span className="text-sm hover:text-indigo-600 flex flex-row space-x-3 text-slate-700">
          {" "}
          Financements
        </span>
        <span className="text-sm hover:text-indigo-600 flex flex-row space-x-3 text-slate-700">
          Experience de formation
        </span>
        <span className="text-sm hover:text-indigo-600 flex flex-row space-x-3 text-slate-700">
          {" "}
          Forum
        </span>
        <span className="text-sm hover:text-indigo-600 flex flex-row space-x-3 text-slate-700">
          {" "}
          Blog{" "}
          <HiOutlineExternalLink
            style={{ marginTop: "2px ", marginLeft: "5px" }}
          />
        </span>
        <span className="text-sm hover:text-indigo-600 flex flex-row space-x-3 text-slate-700">
          {" "}
          Presse{" "}
          <HiOutlineExternalLink
            style={{ marginTop: "2px ", marginLeft: "5px" }}
          />
        </span>
      </div>
      <div className="hidden lg:flex flex-col space-y-3">
        <p className="text-md uppercase hover:text-indigo-600 pb-3">
          {" "}
          Opportunites
        </p>

        <span className="text-sm hover:text-indigo-600 flex flex-row space-x-3 text-slate-700">
          {" "}
          Nous rejoindres{" "}
          <HiOutlineExternalLink
            style={{ marginTop: "2px ", marginLeft: "5px" }}
          />
        </span>
        <span className="text-sm hover:text-indigo-600 flex flex-row space-x-3 text-slate-700">
          {" "}
          Devenir Mentor{" "}
          <HiOutlineExternalLink
            style={{ marginTop: "2px ", marginLeft: "5px" }}
          />
        </span>
        <span className="text-sm hover:text-indigo-600 flex flex-row space-x-3 text-slate-700">
          {" "}
          Devenir Coach arriere{" "}
          <HiOutlineExternalLink
            style={{ marginTop: "2px ", marginLeft: "5px" }}
          />
        </span>
        <p className="text-md uppercase pt-4"> Aide</p>
        <div className="flex flex-row space-x-2">
          <div className="p-2 hover:bg-gray-200 hover:rounded-full cursor-pointer">
            <AiFillMail
              className="hover:text-indigo-500"
              style={{ fontSize: "30px" }}
            />
          </div>
          <div className="w-[0.2px] h-full bg-gray-600 opacity-30"> </div>
          <span className="p-2">
            <p className="text-md uppercase mt-1"> FAQ</p>
          </span>
        </div>
      </div>
      <div className="hidden lg:flex flex-col space-y-3">
        <p className="text-md uppercase pb-3"> Pour les entreprises </p>
        <span className="text-sm  flex flex-row space-x-3 text-slate-700">
          Formation, reconversion et alternance
        </span>
        <p className="text-md uppercase pt-3"> Pour les entreprises </p>
        <span className="text-sm  flex flex-row space-x-3 text-slate-700">
          {" "}
          Financements
        </span>
        <span className="text-sm  flex flex-row space-x-3 text-slate-700">
          Experience de formation
        </span>
        <span className="text-sm  flex flex-row space-x-3 text-slate-700">
          {" "}
          Forum
        </span>
        <span className="text-sm  flex flex-row space-x-3 text-slate-700">
          {" "}
          Blog{" "}
          <HiOutlineExternalLink
            style={{ marginTop: "2px ", marginLeft: "5px" }}
          />
        </span>
        <span className="text-sm  flex flex-row space-x-3 text-slate-700">
          {" "}
          Presse{" "}
          <HiOutlineExternalLink
            style={{ marginTop: "2px ", marginLeft: "5px" }}
          />
        </span>
      </div>
      <div className="hidden lg:flex flex-col space-y-3">
        <p className="text-md uppercase pb-3"> Openclassrooms</p>
        <span className="text-sm  flex flex-row space-x-3 text-slate-700">
          {" "}
          Qui sommes nous
        </span>
        <span className="text-sm  flex flex-row space-x-3 text-slate-700">
          {" "}
          Financements
        </span>
        <span className="text-sm  flex flex-row space-x-3 text-slate-700">
          Experience de formation
        </span>
        <span className="text-sm  flex flex-row space-x-3 text-slate-700">
          {" "}
          Forum
        </span>
        <span className="text-sm  flex flex-row space-x-3 text-slate-700">
          {" "}
          Blog{" "}
          <HiOutlineExternalLink
            style={{ marginTop: "2px ", marginLeft: "5px" }}
          />
        </span>
        <span className="text-sm  flex flex-row space-x-3 text-slate-700">
          {" "}
          Presse{" "}
          <HiOutlineExternalLink
            style={{ marginTop: "2px ", marginLeft: "5px" }}
          />
        </span>
        <span className="text-sm  flex flex-row space-x-3 text-slate-700">
          {" "}
          Blog{" "}
          <HiOutlineExternalLink
            style={{ marginTop: "2px ", marginLeft: "5px" }}
          />
        </span>
        <span className="text-sm  flex flex-row space-x-3 text-slate-700">
          {" "}
          Presse{" "}
          <HiOutlineExternalLink
            style={{ marginTop: "2px ", marginLeft: "5px" }}
          />
        </span>
        <span className="text-sm  flex flex-row space-x-3 text-slate-700">
          {" "}
          Blog{" "}
          <HiOutlineExternalLink
            style={{ marginTop: "2px ", marginLeft: "5px" }}
          />
        </span>
        <span className="text-sm  flex flex-row space-x-3 text-slate-700">
          {" "}
          Presse{" "}
          <HiOutlineExternalLink
            style={{ marginTop: "2px ", marginLeft: "5px" }}
          />
        </span>
      </div>
    </div>
  );
};

export default Footer;
