import React, { useState } from "react";
import logo from "../assets/logo.png";
import { BiChevronDown, BiChevronUp, BiMenu } from "react-icons/bi";
import { BsSearch } from "react-icons/bs";
import { MdSchool } from "react-icons/md";
import ButtonComponent from "../components/ButtonComponent";

const Navbar: React.FC = () => {
  const [openMenu, setOpenMenu] = useState(false);
  return (
    <div className="w-full px-4 py-1 lg:px-0 bg-white border border-b-slate-300 flex justify-between">
      {/* Desktop pov */}
      <div
        className={`absolute ${
          openMenu ? "hidden lg:flex" : "hidden"
        } top-16 p-10 w-screen bg-white border border-t-slate-300 z-10`}
      >
        <div className="w-5/6 flex space-x-10 justify-between ">
          <div className="flex flex-col space-y-1 p-2">
            <p className="font-bold text-lg"> Nos formations</p>
            <p className="text-slate-500 text-xs">
              {" "}
              100% en ligne et à votre rythme.
            </p>
            <p className=" text-sm text-md pt-2 text-indigo-600 underline">
              {" "}
              Comparer nos formations
            </p>
          </div>
          <div className="flex p-2 space-x-3 hover:bg-indigo-200 w-1/3 rounded-md cursor-pointer">
            <div className="rounded-full bg-indigo-600 w-8 h-8 flex mx-auto p-2">
              <MdSchool
                className="text-white text-lg mx-auto"
                style={{ fontSize: "18px" }}
              />
            </div>
            <div className="flex flex-col">
              <p className="font-bold text-md"> Formations diplomantes</p>
              <p className="text-slate-500 text-xs">
                {" "}
                Apprenez un metier d'avenir grace a des projets concrets et un
                mentor individuel
              </p>
            </div>
          </div>
          <div className="w-[0.2px] h-full bg-gray-600 opacity-30"> </div>
          <div className="flex p-2 space-x-3 hover:bg-indigo-200 w-1/3  rounded-md cursor-pointer">
            <div className="rounded-full bg-indigo-600 w-8 h-8 flex mx-auto p-2 ">
              <MdSchool
                className="text-white text-lg mx-auto"
                style={{ fontSize: "18px" }}
              />
            </div>
            <div className="flex flex-col">
              <p className="font-bold text-md"> Formations diplomantes</p>
              <p className="text-slate-500 text-xs">
                {" "}
                Apprenez un metier d'avenir grace a des projets concrets et un
                mentor individuel
              </p>
            </div>
          </div>
        </div>
      </div>
      <div className="hidden lg:pl-10  lg:flex space-x-6 mt-4">
        <img src={logo} alt="" className="w-60 h-8" />

        <div className="flex space-x-6 ">
          <span
            className="flex space-x-2 cursor-pointer"
            onClick={() => {
              setOpenMenu(!openMenu);
            }}
          >
            <a className="hover:text-indigo-600"> Formations </a>
            {!openMenu ? (
              <BiChevronDown style={{ marginTop: "2px", fontSize: "20px" }} />
            ) : (
              <BiChevronUp style={{ marginTop: "2px", fontSize: "20px" }} />
            )}
          </span>
          <div className="w-[0.2px] h-6 bg-gray-600 opacity-30"> </div>
          <span>
            <p className="hover:text-indigo-600">Pour les entreprises</p>
          </span>
        </div>
      </div>
      <div className="hidden lg:pr-10 lg:flex space-x-8 mt-4">
        <div>
          <BsSearch style={{ marginTop: "2px", fontSize: "20px" }} />
        </div>
        <div className="relative bottom-2">
          <ButtonComponent
            text="Se connecter"
            full={false}
            addtionalStyling="text-md px-6 py-2 "
          />
        </div>
      </div>
      {/* Mobile Pov */}
      <div className="flex lg:hidden  space-x-6 mt-4">
        <div>
          <BiMenu style={{ fontSize: "24px" }} />
        </div>
        <img src={logo} alt="" className="w-42 h-8" />
      </div>
      <div className="mt-4 pr-10 flex lg:hidden">
        <BsSearch style={{ fontSize: "24px" }} />
      </div>
    </div>
  );
};

export default Navbar;
