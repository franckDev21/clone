export interface ITask {
    title: string;
    description: string;
    important: boolean;
    date: Date;
}

export interface IInput {
    type: string;
    placeholder: string;
    change: string | Date,
    setChange: any
}