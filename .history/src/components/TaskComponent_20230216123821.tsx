import React from "react";
import { ITaskComponent } from "../interfaces";

const TaskComponent: React.FC<ITaskComponent> = ({
  title,
  description,
  icon,
  date,
}) => {
  return (
    <div className="p-2 border-l-4 border-green-600 flex-row justify-between">
      <span className="flex-col space-y-1">
        <p className="text-xl font-bold"> {title} </p>
      </span>
    </div>
  );
};

export default TaskComponent;
