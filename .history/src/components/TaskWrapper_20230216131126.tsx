import React, { ReactFragment, ReactNode } from "react";

const TaskWrapper: React.FC<{ children: any }> = ({ children }) => {
  return <div className="p-2 h-400 flex-col space-y-2">{children}</div>;
};

export default TaskWrapper;
