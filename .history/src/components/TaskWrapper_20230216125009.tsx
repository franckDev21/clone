import React, { ReactNode } from "react";

const TaskWrapper: React.FC<{ children: ReactNode }> = ({ children }) => {
  return <div className="p-2 h-100 flex-col space-y-2">{children}</div>;
};

export default TaskWrapper;
