import { IPostsData } from "./interfaces";

export default class Store {
  allDatas: Promise<Response>;
  oneData: Promise<Response>;
  constructor() {
    const restClient = (path: string) => {
      return fetch("https://jsonplaceholder.typicode.com/posts/" + path, {
        method: "get",
      });
    };

    this.allDatas = restClient("");
    this.oneData = restClient("");
  }

  fetchDatas() {
    return this.allDatas.then((response) => response.json());
  }

  fetchOneData(data: IPostsData) {
    return this.allDatas;
  }
}
