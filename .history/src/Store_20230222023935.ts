export default class Store {
  restClient: Promise<Response>;
  constructor() {
    this.restClient = fetch("https://jsonplaceholder.typicode.com/posts");
  }

  fetchDatas() {
    return this.restClient.then((response) => response.json());
  }
}
