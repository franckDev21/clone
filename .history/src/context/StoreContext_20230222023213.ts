import React, { createContext } from "react";
import Store from "../Store";

// initialize the context
const StoreContext = createContext({} as unknown as Store);

export default StoreContext;
