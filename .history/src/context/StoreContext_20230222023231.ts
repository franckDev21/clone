import React, { createContext } from "react";
import Store from "../Store";

// initialize the context
const StoreContext = createContext({} as Store);

export default StoreContext;
