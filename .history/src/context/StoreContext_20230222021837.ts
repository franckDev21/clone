import React, { createContext } from "react";

// initialize the context
const StoreContext = createContext({});

export default StoreContext;
