import React, { createContext } from "react";

// initialize the context
const StoreContext = createContext({} as unknown as Store);

export default StoreContext;
