import React, { useState } from "react";

function App() {
  return (
    <div className="font-display p-10 flex justify-center items-center">
      <div className="p-10 shadow-md flex flex-col rounded-sm bg-slate-200"></div>
    </div>
  );
}

export default App;
