export default class Store {
  restClient: Promise<Response>;
  constructor() {
    this.restClient = fetch("https://jsonplaceholder.typicode.com/pots");
  }

  fetchDatas() {
    return this.restClient.then((response) => response.json());
  }
}
