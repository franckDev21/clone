export default class Store {
  restClient: Promise<Response>;
  constructor() {
    this.restClient = fetch("https://jsonplaceholder.typicode.com/todos/1");
  }

  fetchDatas() {
    return this.restClient.then((response) => response.json());
  }
}
